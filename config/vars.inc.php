<?php

include_once($_SERVER['DOCUMENT_ROOT'] . '/classes/loader.php');
spl_autoload_register('loader');

// contains variables which would afford much of logical handling & can be importet if these variables are needed in files
// intialisiese database connection
$connection = new Conn();

// get categorys which user can select
$categorys = $connection->GetCategorys();
$hiddencategorys = $connection->GetHiddenCategorys();
$abonateablecats = $connection->GetAbonatebleCategorys();
$abonatedcategorys = $connection->GetAbonatedCategorys();
$catswhereeditor = $connection->GetCatsWhereEditor();
$userdata = $connection->GetUserData();


if (isset($_SESSION['userdata'])) {
	$isadmin = $_SESSION['userdata'][0];
	$banned = $_SESSION['userdata'][1];
	if (is_array($banned)) {
		$bannreason = $_SESSION['userdata'][1][0];
	}
}
// get links, when session is set, where user selected categorys, get links only from selected categorys
if ($_SESSION['selectedcategorys']) {
	$alllinks = $connection->GetSelectedcatLinks($_SESSION['selectedcategorys']);
} else {
	$alllinks = $connection->GetLinks();
}
?>
