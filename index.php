<?php

session_start();
include_once('classes/loader.php');
include($_SERVER['DOCUMENT_ROOT'] . '/config/vars.inc.php');
spl_autoload_register('loader');
?>

<?php

//index.php sends post & get requests to submit.php which redirects back to index.php with setted session variables
// when session variable for login is true, then display home
if (isset($_SESSION['loggedinyow']) && $_SESSION['loggedinyow'] == "TRUE") {
	if (isset($_SESSION['addrow']) && $_SESSION['loggedinyow'] == "TRUE") {
		$header = new Html('Header');
		$navbar = new Html('Navbar');
		$home = new Html('Addrow');
		$footer = new Html('Footer');
	} else if (!isset($_SESSION['navbar'])) {
		$header = new Html('Header');
		$navbar = new Html('Navbar');
		$home = new Html('Home');
		$footer = new Html('Footer');
	}
// when cookie for autlogin is set & check against database if the encrypted username is correct, if yes, set loginsession true
} else if (isset($_COOKIE['autologin'])) {
	if (password_verify($_SESSION['username'], $_COOKIE['autologin'])) {
		echo "autologin did work and cookie is correct";
		$header = new Html('Header');
		$navbar = new Html('Navbar');
		$home = new Html('Home');
		$footer = new Html('Footer');
		$_SESSION['loggedinyow'] = "TRUE";
	} else {
		echo "autologin did work but cookie is not correct";
	}
// check if i should display registerform
} else if (isset($_SESSION['forwardtoregister'])) {
	$header = new Html('Header');
	$navbar = new Html('Navbar');
	$login = new Html('Register');
	$footer = new Html('Footer');
//tell me if register was successfull or not
} else if (isset($_SESSION['registered'])) {
	if ($_SESSION['registered'] == "registered") {
		echo "register successfull";
		unset($_SESSION['registered']);
	} else if ($_SESSION['registered'] == "passnotmatch") {
		echo "password not match";
	} else if ($_SESSION['registered'] == "userormailexists") {
		echo "Username or email already exists";
	}
	$header = new Html('Header');
	$navbar = new Html('Navbar');
	$login = new Html('Login');
	$footer = new Html('Footer');
// everything else, display home with loginform
} else {
	$header = new Html('Header');
	$navbar = new Html('Navbar');
	$login = new Html('Login');
	$footer = new Html('Footer');
}

if ($_SESSION['loggedinyow'] == "TRUE") {
	switch ($_SESSION['navbar']) {
		case "administration":
			$header = new Html('Header');
			$navbar = new Html('Navbar');
			$administration = new Html('Administration');
			$footer = new Html('Footer');
			break;
		case "home":
			$header = new Html('Header');
			$navbar = new Html('Navbar');
			$home = new Html('Home');
			$footer = new Html('Footer');
			break;
		case "settings":
			$header = new Html('Header');
			$navbar = new Html('Navbar');
			$settings = new Html('Settings');
			$footer = new Html('Footer');
			break;
	}
}
?>