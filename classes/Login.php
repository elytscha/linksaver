<?php

include_once('loader.php');
spl_autoload_register('loader');

// class to handle the login process

class Login {

	private $username;
	private $connection;
	private $uid;
	private $uservalid;

	// when class is initializised with username & password, call the userisvalid function with the username & password & check if result is not empty, if its not empty, login is successful
	public function __construct($username, $password) {
		$this->connection = new Conn();
		$this->isuservalid = $this->connection->UserValid($username);
		$result = $this->connection->getResult();
		$this->username = $result['user'];
		$this->uid = $result['user_id'];
		$passwordfromdb =  $result['password'];
		if (password_verify($password, $passwordfromdb)) {
			$this->uservalid = TRUE;
		}
	}

	public function GetUserData($username) {
		$isadmin = $this->connection->IsAdmin($username);
		$isbanned = $this->connection->IsBanned($username);
		return [$isadmin, $isbanned];
	}

	// function to return the actual proccessed username
	public function GetUsername() {
		return $this->username;
	}

	// function to return the actual processed user id
	public function GetUid() {
		return $this->uid;
	}

	// function to return the successfull or not successfull login try
	public function IsValid() {
		return $this->uservalid;
	}

}

?>
