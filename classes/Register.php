<?php

include_once('loader.php');
spl_autoload_register('loader');
?>

<?php

class Register {

	// initalizise variables
	private $firstname;
	private $lastname;
	private $username;
	private $password;
	private $password2;
	private $email;
	private $userexists;
	private $emailexists;
	private $status;

	// when class is initaliziesed with array registerdata, cast the array values to own variables, spawn a new databaseconnection, check if the user or the email exists & if password matches, if not, reg user, if yes register, if not fill error variable with state
	public function __construct($registerdata) {
		$this->firstname = $registerdata['firstname'];
		$this->lastname = $registerdata['lastname'];
		$this->username = $registerdata['username'];
		$this->password = $registerdata['password'];
		$this->password2 = $registerdata['password2'];
		$this->email = $registerdata['email'];
		$connection = new Conn();
		$this->userexists = $connection->UserExists($this->username);
		$this->emailexists = $connection->EmailExists($this->email);
		if ($this->userexists == false || $this->emailexists == false) {
			if ($this->password == $this->password2) {
				$register = $connection->RegUser($this->firstname, $this->lastname, $this->username, password_hash($this->password, PASSWORD_BCRYPT), $this->email);
				$this->status = "registered";
			} else {
				$this->status = "passnotmatch";
			}
		} else if ($this->userexists == true || $this->emailexists == true) {
			$this->status = "userormailexists";
		}
	}

	// return the var which contains the info if the user exists or not
	public function UserExist() {
		return $this->userexists;
	}

	// return the variable which contains info if the email exists
	public function EmailExists() {
		return $this->emailexists;
	}

	// return the variable which contains the info about the regisater status, registered, passnotmatch or userormailexists
	public function RegisterStatus() {
		return $this->status;
	}

}
?>

