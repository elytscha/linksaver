<?php

// class for display html files which are taken from html/$Name.html
class Html {

	//when class is intializiesed with Name of the html file, include the html into an object to avoid '' to make strange things & echo the object which then contains the pure html
	public function __construct($html) {
		$file = $_SERVER['DOCUMENT_ROOT'] . "/html/$html.html";
		ob_start();
		include($file);
		$out1 = ob_get_clean();
		if (isset($bannreason)) {
			echo "You are banned because of " . $bannreason . " and will now be logged off<br>";
			header("refresh:3;url=/php/submit.php");
		} else {
			echo $out1;
		}
	}

}

?>