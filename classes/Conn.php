<?php

// handles the database connection & do the querys which other functions can ask for, sqli protected

class Conn {

	// initialisies variables
	private $pdo;
	private $statement;
	public $result;
	private $fetch;
	private $userexists = false;
	private $emailexists = false;
	private $userid;

	// function which spawns a pdo connection
	public function __construct() {
		include($_SERVER['DOCUMENT_ROOT'] . '/config/config.inc.php');
		try {
			$this->pdo = new PDO("mysql:host={$db_host};dbname={$db_name}", $db_user, $db_pass);
			$this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		} catch (PDOException $e) {
			echo $e->getMessage();
		}
	}

	// check if the given userdata (name & password) is matching the data which is stored in the database & add the result to the $result var
	public function UserValid($username) {
		$uservalid = 'SELECT user_id, user, password FROM user WHERE user = :username';
		$runuserexist = $this->pdo->prepare($uservalid, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
		$runuserexist->execute(array(':username' => $username));
		$this->result = $runuserexist->fetch();
	}

	// check if the user which is given to the function exists in the database, if the result is not empty, the user exists, so $userexists var gets setted true
	public function UserExists($username) {
		$uservalid = 'SELECT user_id FROM user WHERE user = :username';
		$runuserexist = $this->pdo->prepare($uservalid, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
		$runuserexist->execute(array(':username' => $username));
		$result = $runuserexist->fetch();
		if (!empty($result['user_id'])) {
			return $this->userexists = true;
		} else {
			return $this->userexists;
		}
	}

	// check if the given email exists in the database, if yes, return emailexsits = true
	public function EmailExists($email) {
		$emailvalid = 'SELECT user_id FROM user WHERE email = :email';
		$runemailexist = $this->pdo->prepare($emailvalid, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
		$runemailexist->execute(array(':email' => $email));
		$result = $runemailexist->fetch();
		if (!empty($result['email'])) {
			return $this->emailexists = true;
		} else {
			return $this->emailexists;
		}
	}

	//register a user with the given data from the registerform, if something fails, throw the error
	public function RegUser($firstname, $lastname, $username, $password, $email) {
		$regquery = 'INSERT INTO user (user,password,email,firstname,lastname) VALUES (:username, :password, :email, :firstname, :lastname)';
		$runregister = $this->pdo->prepare($regquery, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
		try {
			$runregister->execute(array(':username' => $username, ':password' => $password, ':email' => $email, ':firstname' => $firstname, ':lastname' => $lastname));
		} catch (PDOException $e) {
			echo $e->getMessage();
		}
	}

	//function to add links and send mail to member who have subscribed
	public function AddLink($username, $category, $title, $wwwlink) {
		$addlinkquery = 'INSERT INTO links (user_id, cat_id, title, link) VALUES (:uid, :catid, :title, :wwwlink)';
		$uid = $this->GetUID($username);
		$catid = $this->GetCatID($category);
		$mail = new Mailer();

		$runaddlinkquery = $this->pdo->prepare($addlinkquery, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
		$checkiflinkexists = 'SELECT link FROM links WHERE link = :link';
		$checklink = $this->pdo->prepare($checkiflinkexists, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
		$checklink->execute(array(':link' => $wwwlink));
		$linkchecked = $checklink->fetch();
		$result = false;
		if (empty($linkchecked['link'])) {
			$result = $runaddlinkquery->execute(array(':uid' => $uid, ':catid' => $catid, ':title' => $title, ':wwwlink' => $wwwlink));
			$getmailstomail = $this->pdo->prepare('SELECT email FROM subscription INNER JOIN user ON subscription.user_id = user.user_id INNER JOIN category ON subscription.cat_id = category.cat_id where subscription.cat_id = :cat_id');
			$getmailstomail->execute(array(':cat_id' => $catid));
			$mailstosend = $getmailstomail->fetchAll(PDO::FETCH_COLUMN, 0);
			$mail->SendMail($mailstosend, $category, $wwwlink);
			return $result;
		} else {
			return $result;
		}
	}

	public function ValidateLink($linkid) {
		$linktoval = $this->pdo->prepare('UPDATE links SET status = "activated" WHERE link_id = :link_id');
		if ($_SESSION['userdata'][0] == "true") {
			$result = $linktoval->execute(array(':link_id' => $linkid));
			return $result;
		} else {
			$uid = $this->GetUID($_SESSION['username']);
			$getlinkcat = $this->pdo->prepare('SELECT cat_id FROM links WHERE link_id = :link_id');
			$getlinkcat->execute(array(':link_id' => $linkid));
			$linkcatis = $getlinkcat->fetchAll();
			$linkcat = $linkcatis[0][0];
			$iseditor = $this->pdo->prepare('SELECT user_id FROM editors WHERE user_id = :user_id AND cat_id = :cat_id AND is_editor = "TRUE"');
			$iseditor->execute(array(':user_id' => $uid, ':cat_id' => $linkcat));
			$candeletelink = $iseditor->fetchAll();
			if (empty($candeletelink)) {
				return "you are not an editor for this category, so you can't validate this link";
			} else {
				$result = $linktoval->execute(array(':link_id' => $linkid));
				return $result;
			}
		}
	}

	//function to return user id which gets a username
	private function GetUID($username) {
		$getuserid = ('SELECT user_id FROM user WHERE user = :username');
		$runuid = $this->pdo->prepare($getuserid, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
		$runuid->execute(array(':username' => $username));
		$userid = $runuid->fetch();
		if (!empty($userid['user_id'])) {
			return $userid['user_id'];
		} else {
			return $userid = "notfound";
		}
	}

	//function to return cat id which gets a category
	private function GetCatID($category) {
		$getcatid = ('SELECT cat_id FROM category WHERE name = :category');
		$runcatid = $this->pdo->prepare($getcatid, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
		$runcatid->execute(array(':category' => $category));
		$catid = $runcatid->fetch();
		return $catid['cat_id'];
	}

	//return userdata like email, name, lastname etc
	public function GetUserData() {
		$userdata = $this->pdo->prepare('SELECT email, firstname, lastname FROM user WHERE user = :username');
		$userdata->execute(array(':username' => $_SESSION['username']));
		$result = $userdata->fetchAll();
		return $result;
	}

	//get all categorys the user can see
	public function GetCategorys() {
		$categorys = $this->pdo->prepare('SELECT name FROM category WHERE hidden = "false"');
		$categorys->execute();
		$unhiddencatresult = $categorys->fetchAll(PDO::FETCH_COLUMN, 0);
		$username = $_SESSION['username'];
		$hiddencategorys = $this->pdo->prepare('SELECT name FROM category INNER JOIN editors ON category.cat_id = editors.cat_id INNER JOIN user ON user.user_id = editors.user_id WHERE hidden = "true" AND user.user = :username');
		$hiddencategorys->execute(array(':username' => $username));
		$hiddencategorys->execute();
		$hiddencatresult = $hiddencategorys->fetchAll(PDO::FETCH_COLUMN, 0);
		$this->result = array_merge($unhiddencatresult, $hiddencatresult);
		return $this->result;
	}

	// function to get hidden categorys
	public function GetHiddenCategorys() {
		$categorys = $this->pdo->prepare('SELECT name FROM category WHERE hidden = "true"');
		$categorys->execute();
		$hiddencatresult = $categorys->fetchAll(PDO::FETCH_COLUMN, 0);
		$this->result = $hiddencatresult;
		return $this->result;
	}

	public function GetCatsWhereEditor() {
		$uid = $this->GetUID($_SESSION['username']);
		$categorys = $this->pdo->prepare('SELECT name FROM category INNER JOIN editors ON category.cat_id = editors.cat_id WHERE is_editor = "true" AND user_id = :user_id');
		$categorys->execute(array(':user_id' => $uid));
		$editorcats = $categorys->fetchAll(PDO::FETCH_COLUMN, 0);
		$this->result = $editorcats;
		return $this->result;
	}

	public function GetAbonatebleCategorys() {
		$username = $_SESSION['username'];
		$abonateablepublic = $this->pdo->prepare('SELECT name FROM category WHERE hidden = "false"');
		$abonateablepublic->execute();
		$abonateablecatspublic = $abonateablepublic->fetchAll();
		$abonateableprivate = $this->pdo->prepare('SELECT name FROM category INNER JOIN editors ON category.cat_id = editors.cat_id INNER JOIN user ON user.user_id = editors.user_id WHERE hidden = "true" AND user.user = :username');
		$abonateableprivate->execute(array(':username' => $username));
		$abonateablecatsprivate = $abonateableprivate->fetchAll();
		$abonateablecats = array_merge($abonateablecatspublic, $abonateablecatsprivate);
		return $abonateablecats;
	}

	public function GetAbonatedCategorys() {
		$username = $_SESSION['username'];
		$abonated = $this->pdo->prepare('SELECT name FROM user INNER JOIN subscription ON user.user_id = subscription.user_id INNER JOIN category ON subscription.cat_id = category.cat_id WHERE user.user = :username AND subscription.subscribed = TRUE');
		$abonated->execute(array(':username' => $username));
		$abonatedcats = $abonated->fetchAll();
		return $abonatedcats;
	}

	public function AbonateCategory($categorys, $username) {
		$uid = $this->GetUID($username);
		foreach ($categorys as $category) {
			$catid = $this->GetCatID($category);
			$checkifexists = $this->pdo->prepare('SELECT id_subscription FROM subscription WHERE cat_id = :cat_id AND user_id = :user_id');
			$addsubscription = $this->pdo->prepare("INSERT INTO subscription (cat_id, user_id, subscribed) VALUES (:cat_id, :user_id, TRUE)");
			$updatesubscription = $this->pdo->prepare('UPDATE subscription SET subscribed = TRUE WHERE user_id = :user_id AND cat_id = :cat_id');
			$checkifexists->execute(array(':cat_id' => $catid, ':user_id' => $uid));
			$checkit = $checkifexists->fetch();
			if (empty($checkit)) {
				$result = $addsubscription->execute(array(':cat_id' => $catid, ':user_id' => $uid));
			} else {
				$result = $updatesubscription->execute(array(':cat_id' => $catid, ':user_id' => $uid));
			}
		}
	}

	public function DeabonateCategory($categorys, $username) {
		$uid = $this->GetUID($username);
		foreach ($categorys as $category) {
			$catid = $this->GetCatID($category);
			$checkifexists = $this->pdo->prepare('SELECT id_subscription FROM subscription WHERE cat_id = :cat_id AND user_id = :user_id');
			$addsubscription = $this->pdo->prepare("INSERT INTO subscription (cat_id, user_id, subscribed) VALUES (:cat_id, :user_id, FALSE)");
			$updatesubscription = $this->pdo->prepare('UPDATE subscription SET subscribed = FALSE WHERE user_id = :user_id AND cat_id = :cat_id');
			$checkifexists->execute(array(':cat_id' => $catid, ':user_id' => $uid));
			$checkit = $checkifexists->fetch();
			if (empty($checkit)) {
				$result = $addsubscription->execute(array(':cat_id' => $catid, ':user_id' => $uid));
			} else {
				$result = $updatesubscription->execute(array(':cat_id' => $catid, ':user_id' => $uid));
			}
		}
	}

	public function AddCategory($catname, $cattype, $usernamefromsession) {
		$addcategory = $this->pdo->prepare("INSERT INTO category (name, hidden) VALUES (:catname, :cattype)");
		$isadmin = $this->IsAdmin($usernamefromsession);
		if ($isadmin == true) {
			try {
				$result = $addcategory->execute(array(':catname' => $catname, ':cattype' => $cattype));
			} catch (PDOException $e) {
				$result = false;
			}
		} else {
			$result = "noadmin";
		}
		return $result;
	}

	public function BannUser($username, $reason, $usernamefromsession) {
		$isadmin = $this->IsAdmin($usernamefromsession);
		$bannuser = $this->pdo->prepare('UPDATE user SET banned = 1, bannreason = :bannreason WHERE user = :username AND banned = 0');
		if ($isadmin == true) {
			try {
				$result = $bannuser->execute(array(':bannreason' => $reason, ':username' => $username));
			} catch (PDOException $e) {
				$result = false;
			}
		} else {
			$result = "noadmin";
		}
		return $result;
	}

	//get linkdata as array to display the links in home when no cat is selected
	public function GetLinks() {
		$links = $this->pdo->prepare('SELECT user.user,category.name,links.title,links.link,links.status,links.link_id FROM links INNER JOIN user ON links.user_id = user.user_id INNER JOIN category ON links.cat_id = category.cat_id');
		$links->execute();
		$this->result = $links->fetchAll();
		return $this->result;
	}

	public function DelLink($linkid) {
		$linktodel = $this->pdo->prepare('DELETE FROM links WHERE link_id = :link_id');
		if ($_SESSION['userdata'][0] == "true") {
			$result = $linktodel->execute(array(':link_id' => $linkid));
			return $result;
		} else {
			$uid = $this->GetUID($_SESSION['username']);
			$getlinkcat = $this->pdo->prepare('SELECT cat_id FROM links WHERE link_id = :link_id');
			$getlinkcat->execute(array(':link_id' => $linkid));
			$linkcatis = $getlinkcat->fetchAll();
			$linkcat = $linkcatis[0][0];
			$iseditor = $this->pdo->prepare('SELECT user_id FROM editors WHERE user_id = :user_id AND cat_id = :cat_id AND is_editor = "TRUE"');
			$iseditor->execute(array(':user_id' => $uid, ':cat_id' => $linkcat));
			$candeletelink = $iseditor->fetchAll();
			if (empty($candeletelink)) {
				return "you are not an editor for this category, so you can't delete this link";
			} else {
				$result = $linktodel->execute(array(':link_id' => $linkid));
				return $result;
			}
		}
	}

	//when user selects categorys get links with selected categorys to display them in home
	public function GetSelectedcatLinks($selectedcategorys) {
		$links = $this->pdo->prepare('SELECT user.user,category.name,links.title,links.link,links.status,links.link_id FROM links INNER JOIN user ON links.user_id = user.user_id INNER JOIN category ON links.cat_id = category.cat_id WHERE category.name IN ("' . implode('","', $selectedcategorys) . '")');
		$links->execute();
		$this->result = $links->fetchAll();
		return $this->result;
	}

	public function IsAdmin($username) {
		$checkifadminq = $this->pdo->prepare('SELECT user FROM user WHERE user = :username AND is_admin = "true"');
		$checkifadminq->execute(array(':username' => $username));
		$result = $checkifadminq->fetch();
		if (empty($result['user'])) {
			return $result = "false";
		} else {
			return $result = "true";
		}
	}

	public function IsBanned($username) {
		$checkbforbann = $this->pdo->prepare('SELECT user, bannreason FROM user WHERE user = :username AND banned = 1');
		$checkbforbann->execute(array(':username' => $username));
		$result = $checkbforbann->fetch();
		if (empty($result['user'])) {
			return $result = "false";
		} else {
			$result = [$result['bannreason'], "true"];
			return $result;
		}
	}

	public function AddUsertoCat($username, $category, $usernamefromsession) {
		$addusertocatquery = $this->pdo->prepare('INSERT INTO editors (user_id, cat_id, is_editor) VALUES (:user_id, :cat_id, "false")');
		$validateit = $this->pdo->prepare('SELECT edit_id FROM editors WHERE user_id = :user_id AND cat_id = :cat_id AND is_editor = "false"');
		$updateifexists = $this->pdo->prepare('SELECT edit_id FROM editors WHERE user_id = :user_id AND cat_id = :cat_id');
		$updatequery = $this->pdo->prepare('UPDATE editors SET is_editor = "false" WHERE cat_id = :cat_id AND user_id = :user_id');
		$uid = $this->GetUID($username);
		$catid = $this->GetCatID($category);
		$isadmin = $this->IsAdmin($usernamefromsession);
		$validateit->execute(array(':user_id' => $uid, ':cat_id' => $catid));
		$validatresult = $validateit->fetch();
		$updateifexists->execute(array(':user_id' => $uid, ':cat_id' => $catid));
		$updateifexistsresult = $updateifexists->fetch();
		if ($isadmin == "true") {
			if (empty($validatresult['edit_id'])) {
				if ($uid == "notfound") {
					return $result = "usernotfound";
				} else {
					if (empty($updateifexistsresult['edit_id'])) {
						$result = $addusertocatquery->execute(array(':user_id' => $uid, ':cat_id' => $catid));
					} else {
						$result = $updatequery->execute(array(':user_id' => $uid, ':cat_id' => $catid));
					}
					return $result = "worked";
				}
			} else {
				return $result = "exists";
			}
		} else {
			return $result = "noadmin";
		}
	}

	public function AddUsertoCatAsEditor($username, $category, $usernamefromsession) {
		$addusertocatquery = $this->pdo->prepare('INSERT INTO editors (user_id, cat_id, is_editor) VALUES (:user_id, :cat_id, "true")');
		$validateit = $this->pdo->prepare('SELECT edit_id FROM editors WHERE user_id = :user_id AND cat_id = :cat_id AND is_editor = "true"');
		$updateifexists = $this->pdo->prepare('SELECT edit_id FROM editors WHERE user_id = :user_id AND cat_id = :cat_id');
		$updatequery = $this->pdo->prepare('UPDATE editors SET is_editor = "true" WHERE cat_id = :cat_id AND user_id = :user_id');
		$uid = $this->GetUID($username);
		$catid = $this->GetCatID($category);
		$isadmin = $this->IsAdmin($usernamefromsession);
		$validateit->execute(array(':user_id' => $uid, ':cat_id' => $catid));
		$validatresult = $validateit->fetch();
		$updateifexists->execute(array(':user_id' => $uid, ':cat_id' => $catid));
		$updateifexistsresult = $updateifexists->fetch();
		if ($isadmin == "true") {
			if (empty($validatresult['edit_id'])) {
				if ($uid == "notfound") {
					return $result = "usernotfound";
				} else {
					if (empty($updateifexistsresult['edit_id'])) {
						$result = $addusertocatquery->execute(array(':user_id' => $uid, ':cat_id' => $catid));
					} else {
						$result = $updatequery->execute(array(':user_id' => $uid, ':cat_id' => $catid));
					}
					return $result = "worked";
				}
			} else {
				return $result = "exists";
			}
		} else {
			return $result = "noadmin";
		}
	}

	//function to get the acutal result
	public function getResult() {
		return $this->result;
	}

	//function to get the status of the query which was applied against the database
	public function getFetch() {
		return $this->fetch;
	}

}

?>
