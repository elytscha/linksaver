<?php

//autoloader to load the functions, to avoid includes in every file
function loader($classname) {
	$filename = __DIR__ . '/' . $classname . '.php';
	require_once($filename);
}

?>
