<?php

include_once('loader.php');
spl_autoload_register('loader');
?>

<?php

class Administration {

	public $status;
	private $connection;
	private $usernamefromsession;

	public function __construct() {
		$this->connection = new Conn();
		$this->usernamefromsession = $_SESSION['username'];
	}

	public function AddUsertoCat($username, $category) {
		$addusertocat = $this->connection->AddUserToCat($username, $category, $this->usernamefromsession);
		return $addusertocat;
	}

	public function AddUserAsEditortoCat($username, $category) {
		$usernamefromsession = $_SESSION['username'];
		$addusertocat = $this->connection->AddUserToCatAsEditor($username, $category, $this->usernamefromsession);
		return $addusertocat;
	}

	public function callAddCategory($name, $type) {
		$addcategory = $this->connection->AddCategory($name, $type, $this->usernamefromsession);
		return $addcategory;
	}

	public function callBannUser($name, $reason) {
		$bannuser = $this->connection->BannUser($name, $reason, $this->usernamefromsession);
		return $bannuser;
	}

}
?>
