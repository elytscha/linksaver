<?php

include_once('loader.php');
spl_autoload_register('loader');
require '/usr/share/php/libphp-phpmailer/PHPMailerAutoload.php';

class Mailer {

	private $mail;

	public function __construct() {
		include($_SERVER['DOCUMENT_ROOT'] . '/config/config.inc.php');
		$this->mail = new PHPMailer(); // create a new object
		$this->mail->IsSMTP(); // enable SMTP
		$this->mail->SMTPDebug = 0; // debugging: 1 = errors and messages, 2 = messages only
		$this->mail->SMTPAuth = true; // authentication enabled
		$this->mail->SMTPSecure = 'ssl'; // secure transfer enabled REQUIRED for Gmail
		$this->mail->Host = $smtp_server;
		$this->mail->Port = $smtp_port; // or 587
		$this->mail->IsHTML(true);
		$this->mail->Username = $sender_email; // username
		$this->mail->Password = $email_pass; // password
		$this->mail->SetFrom($sender_email); // email wihich to send from
	}

	public function SendMail($mailstosend, $category, $link) {
		$this->mail->Subject = "New Link added to $category";
		$this->mail->Body = "Hello, <br> a new link has been added to category: $category which you have subscribed <br> $link";
		foreach ($mailstosend as $newmail) {
			$this->mail->AddAddress($newmail);
			if (!$this->mail->send()) {
				;
			} else {
				;
			}
		}
		$this->mail->clearAddresses();
	}

}
