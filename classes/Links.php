<?php

include_once('loader.php');
spl_autoload_register('loader');

// class to handle links

class Links {

	public $status = '';

	//when class is created with array link, try to add it to table links
	public function __construct($link) {
		$conn = new Conn();
		$username = $_SESSION['username'];
		$category = $link['category'];
		$title = $link['title'];
		$wwwlink = $link['link'];
		if (filter_var($wwwlink, FILTER_VALIDATE_URL) === FALSE) {
			$this->status = 'wronglink';
		} else {
			$status = $conn->AddLink($username, $category, $title, $wwwlink);
			if ($status == true) {
				$this->status = "true";
			} else {
				$this->status = "false";
			}
		}
	}

	public function ReturnLinkState() {
		return $this->status;
	}

}

?>