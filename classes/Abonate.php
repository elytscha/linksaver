<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Abonate
 *
 * @author elias
 */
class Abonate {

	public $connection;

	public function __construct() {
		$this->connection = new Conn();
	}

	public function AbonateCategorys($categorys) {
		$username = $_SESSION['username'];
		$this->connection->AbonateCategory($categorys, $username);
		return $this->connection;
	}

	public function DeabonateCategorys($categorys) {
		$username = $_SESSION['username'];
		$this->connection->DeabonateCategory($categorys, $username);
		return $this->connection;
	}

}
