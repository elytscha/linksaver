<?php

//display array of links as link array
foreach ($alllinks as $onerow) {
	$counter = 0;
	$linkid = $onerow['link_id'];
	$catname = $onerow['name'];
	//display fields from link array
	foreach ($onerow as $onefield) {
		// if link is activated display him in tab activated
		if (in_array("activated", $onerow)) {
			$status = '<tr data-status="completed">';
		}
		// if link is pending display him in tab pending
		if (in_array("pending", $onerow)) {
			$status = '<tr data-status="pending">';
		}
		// if counter 0, 12, 24, 36, ... start new table row
		if ($counter % 10 == 12 || $counter == 0) {
			if (in_array($catname, $catswhereeditor) || $_SESSION['userdata'][0] == "true") {
				if (in_array("pending", $onerow)) {
					$validateit = '<a href="php/submit.php?validatelink=' . $linkid . '" class="btn btn-success validatebutton"><span class="glyphicon glyphicon-thumbs-up"aria-hidden="true"></span></a>';
				} else {
					$validateit = NULL;
				}
				echo $status . '<td align="center">' . $validateit . '<a href="php/submit.php?dellink=' . $linkid . '" class="btn btn-danger"><span class="glyphicon glyphicon-trash"aria-hidden="true"></span></a></td>';
			} else {
				echo $status . '<td></td>';
			}
		}
		// if counter 0, 2, 4, ... display field from array as one tabledata
		if ($counter % 2 == 0 && ($onefield !== "pending" && $onefield !== "activated")) {
			if (is_numeric($onefield)) {
				;
			} else {
				if (filter_var($onefield, FILTER_VALIDATE_URL) === FALSE) {
					echo "<td>$onefield</td>";
				} else {
					echo "<td>" . '<a href="' . $onefield . '">' . $onefield . "</a></td>";
				}
			}
		}
		// if counter 7, 17, 27, ... end tablerow
		if ($counter % 10 == 7) {
			echo "</tr>";
		}
		// counter +1
		$counter++;
	}
}
?>
