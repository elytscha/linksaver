<?php

// start session
session_start();
include($_SERVER['DOCUMENT_ROOT'] . '/config/vars.inc.php');
?>
<?php

// submit php handles every post or get from index.php, set the sessions & variables & redirect back to index.php
// include loader to get classes
include_once('../classes/loader.php');
spl_autoload_register('loader');
// in the start set session loggedin to false
$_SESSION['loggedin'] = false;
// cookie time to store remember me cookie on pc (30 days)
$cookie_time = (3600 * 24 * 30);

// when user logs in check if userlogintry is successfull if yes, set loggedin session & redirect to index.php
if (isset($_POST['login'])) {
	$username = $_POST['username'];
	$password = $_POST['password'];
	$logintry = new Login($username, $password);
	$validlogin = $logintry->IsValid();
	if ($validlogin == FALSE) {
		echo "logindata incorrect";
		header("refresh:3;url=../index.php");
		exit();
	} else {
		echo "sucessfully logged in";
		if (isset($_POST['rememberme'])) {
			$_SESSION['username'] = $logintry->GetUsername();
			$_SESSION['loggedinyow'] = "TRUE";
			setcookie('autologin', password_hash($username, PASSWORD_BCRYPT), time() + $cookie_time);
			$_SESSION['navbar'] = "home";
		} else {
			$_SESSION['userdata'] = $logintry->GetUserData($logintry->GetUsername());
			$_SESSION['loggedinyow'] = "TRUE";
			$_SESSION['username'] = $logintry->GetUsername();
			$_SESSION['navbar'] = "home";
		}
		header('Location: ../index.php');
		exit();
	}
// when logout button gets clicked, unset sessions & destroy session & redirect to index.php
} else if (isset($_POST['logout']) || isset($bannreason)) {
	$_SESSION = array();
	if (isset($_COOKIE['autologin'])) {
		unset($_COOKIE['autologin']);
	}
	session_destroy();
	header('Location: ../index.php');
	exit;
// when registerbutton gets clicked, set session for index.php, that it knows to display the register form
} else if (isset($_POST['forwardtoregister'])) {
	$_SESSION['forwardtoregister'] = "yes";
	if (isset($_SESSION['navbar'])) {
		unset($_SESSION['navbar']);
	}
	header('Location: ../index.php');
	exit();
// when register button in registerform is submitted, register user, when every exemplary check is passed successfull
} else if (isset($_POST['register'])) {
	$registerdata = array('firstname' => $_POST['firstname'], 'lastname' => $_POST['lastname'], 'username' => $_POST['username'], 'password' => $_POST['password'], 'password2' => $_POST['password2'], 'email' => $_POST['email'],);
	$register = new Register($registerdata);
	$status = $register->RegisterStatus();
	unset($_SESSION['forwardtoregister']);
	$_SESSION['registered'] = $status;
	header('Location: ../index.php');
	exit();
	// when homebutton is clicked, redirect to index.php & unset session from other forms and set home session
} else if (isset($_GET['home'])) {
	// unset registered & forwardtoregister sessions that they dont trigger registerformdisplay

	if (isset($_SESSION['forwardtoregister'])) {
		unset($_SESSION['forwardtoregister']);
	}
	if (isset($_SESSION['registered'])) {
		unset($_SESSION['registered']);
	}
	if (isset($_SESSION['addrow'])) {
		unset($_SESSION['addrow']);
	}
	if (isset($_SESSION['showhiddencats'])) {
		unset($_SESSION['showhiddencats']);
	}

	$_SESSION['navbar'] = "home";
	header('Location: ../index.php');
	exit();

// when user selects categorys & submit the select categorys button then set session with selected categorys (array)
} else if (isset($_POST['addrow'])) {
	$_SESSION['addrow'] = true;
	if (isset($_SESSION['navbar'])) {
		unset($_SESSION['navbar']);
	}
	header('Location: ../index.php');
	exit();
// add link to category & return successful or not state
} else if (isset($_POST['addlinktocat'])) {
	$link = array('username' => $_SESSION['username'], 'category' => $_POST['addlinktocategory'], 'title' => $_POST['title'], 'link' => $_POST['link']);
	$addlinktocat = new Links($link);
	$addlinkstate = $addlinktocat->ReturnLinkState();
	switch ($addlinkstate) {
		case "wronglink":
			echo "Link is not a valid url";
			break;
		case "true":
			echo "link successfully added";
			if (isset($_SESSION['addrow'])) {
				unset($_SESSION['addrow']);
			}
			break;
		case "false":
			echo "Something went wrong, please try again";
			break;
	}
	header("refresh:3;url=../index.php");
	exit();
// set session with selected categorys
} else if (isset($_POST['selectedcategorys'])) {
	$_SESSION['selectedcategorys'] = $_POST['selectedcategorys'];
	header('Location: ../index.php');
	exit();

// navigation, set session with administration, settings
} else if (isset($_GET['administration'])) {
	$_SESSION['navbar'] = "administration";
	$_SESSION['showhiddencats'] = true;
	if (isset($_SESSION['addrow'])) {
		unset($_SESSION['addrow']);
	}
	header('Location: ../index.php');
	exit();
} else if (isset($_GET['settings'])) {
	$_SESSION['navbar'] = "settings";
	if (isset($_SESSION['addrow'])) {
		unset($_SESSION['addrow']);
	}
	if (isset($_SESSION['showhiddencats'])) {
		unset($_SESSION['showhiddencats']);
	}
	header('Location: ../index.php');
	exit();
} else if (isset($_POST['addusertocat'])) {
	if (!empty($_POST['userasreader'])) {
		$user = $_POST['userasreader'];
		$category = $_POST['addusertocatory'];
		$admin = New Administration();
		$addusertocat = $admin->AddUsertoCat($user, $category);
		if ($addusertocat == "worked") {
			echo "User $user added as reader to $category";
		} else if ($addusertocat == "exists") {
			echo "User $user already is a reader in $category";
		} else {
			echo "You are not allowed to do this";
		}
	} else {
		echo "No Username given, pls do again";
	}
	header("refresh:3;url=../index.php");
	$_SESSION['showhiddencats'] = true;
	exit();
} else if (isset($_POST['adduseraseditortocat'])) {
	if (!empty($_POST['useraseditor'])) {
		$user = $_POST['useraseditor'];
		$category = $_POST['addusertocategoryasedit'];
		$admin = New Administration();
		$addusertocat = $admin->AddUserAsEditortoCat($user, $category);
		if ($addusertocat == "worked") {
			echo "User $user added as editor to $category";
		} else if ($addusertocat == "exists") {
			echo "User $user already is a editor in $category";
		} else {
			echo "You are not allowed to do this";
		}
	} else {
		echo "No Username given, pls do again";
	}
	header("refresh:3;url=../index.php");
	$_SESSION['showhiddencats'] = true;
	exit();
} else if (isset($_POST['addcat'])) {
	if (!empty($_POST['addcatname'])) {
		$catname = $_POST['addcatname'];
		$cattype = $_POST['privateorpublic'];
		$admin = New Administration();
		$addcat = $admin->callAddCategory($catname, $cattype);
		if ($addcat == true) {
			if ($cattype == "true") {
				echo "Private category $catname added";
			} else {
				echo "Public category $catname added";
			}
		} else if ($addusertocat == "noadmin") {
			echo "You are not allowed to do this";
		} else {
			echo "Category $catname exists already";
		}
	} else {
		echo "No Categoryname given, pls do again";
	}
	header("refresh:3;url=../index.php");
	$_SESSION['showhiddencats'] = true;
	exit();
} else if (isset($_POST['bannuser'])) {
	if (!empty($_POST['bannusername']) && !empty($_POST['bannreason'])) {
		$username = $_POST['bannusername'];
		$reason = $_POST['bannreason'];
		$admin = New Administration();
		$bannuser = $admin->callBannUser($username, $reason);
		if ($bannuser == true) {
			echo "User $username banned";
		} else if ($bannuser == "noadmin") {
			echo "You are not allowed to do this";
		} else {
			echo "Username not found";
		}
	} else {
		echo "No Username or Bannreason given";
	}
	header("refresh:3;url=../index.php");
	$_SESSION['showhiddencats'] = true;
	exit();
} else if (isset($_POST['subscribe']) || isset($_POST['desubscribe'])) {
	$categorys = $_POST['categorys'];
	$abonate = New Abonate();
	if (isset($_POST['subscribe'])) {
		$abonate->AbonateCategorys($categorys);
		echo "selected categorys subscribed, you will be informed about new links per mail, redirecting back";
	} else {
		$abonate->DeabonateCategorys($categorys);
		echo "selected categorys unsubscribed, redirecting back";
	}
	header("refresh:3;url=../index.php");
} else if (isset($_GET['dellink']) || isset($_GET['validatelink'])) {
	$conn = new Conn();
	if (isset($_GET['dellink'])) {
		$dellink = $conn->DelLink($_GET['dellink']);
		if (is_bool($dellink)) {
			echo "Link deleted";
		} else {
			echo $dellink;
		}
	} else {
		$validatelink = $conn->ValidateLink($_GET['validatelink']);
		if (is_bool($validatelink)) {
			echo "Link validated";
		} else {
			echo $validatelink;
		}
	}
	header("refresh:3;url=../index.php");
	exit();
} else {
	echo "Oops, something went wrong, returning to home";
	header("refresh:3;url=../index.php");
	exit();
}
?>
